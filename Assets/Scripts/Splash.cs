﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Splash : MonoBehaviour
{
    public Image splashImage;
    public string loadNextScene;

    IEnumerator Start()
    {
        splashImage.canvasRenderer.SetAlpha(0.0f);

        FadeIn();
        yield return new WaitForSeconds(2.0f);
        FadeOut();
        yield return new WaitForSeconds(2.5f);
        SceneManager.LoadScene("Menu");
    }
    void FadeIn()
    {
        splashImage.CrossFadeAlpha(1.0f, 2.0f,false);
    }
    void FadeOut()
    {
        splashImage.CrossFadeAlpha(0.0f, 2.5f, false);
    }
}
