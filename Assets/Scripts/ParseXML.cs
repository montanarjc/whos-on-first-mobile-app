﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using UnityEngine;
using System.IO;
using System.Xml;
using UnityEngine.UI;
using System;

public class ParseXML : MonoBehaviour
{
    private struct TeamVisitors
    {
        public string playerName;
        public string playerNumber;
        public string playerIndex;
        public string playerPosition;
    }
    private struct TeamHome
    {
        public string playerName;
        public string playerNumber;
        public string playerIndex;
        public string playerPosition;
    }

    private struct Player
    {
        public string playerName;
        public int playerNumber;
        public int playerIndex;
        public string playerPosition;
        public int atBat;
        public int hits;
    }

    private struct Bases
    {
        public string playerOn;
        public int onIndex;
        public string playerWasOn;
        public int wasOnIndex;  
    }

    public int PlayDelay=1;
    public Text txtVisitingTeam;
    public Text txtHomeTeam;
    public Text txtBatter;
    public Text HomeScore;
    public Text VisitorScore;
    public Text ScrollingLine1;
    public Text ScrollingLine2;
    public Text ScrollingLine3;
    public Text Outs;
    public int inning = 0;
    public int hScore=0;
    public int vScore = 0;
    public int atBat = 0; //Visitor=1, Home=2
    private int batter = 0;
    private int outs = 0;
    public string attributeType = "";
    public int[,] score = new int[2,20];
    Bases[] structBase = new Bases[4];
    Player[] players = new Player[40];


    // Use this for initialization
    void Start() {
        if (CheckIfDataExists())
        {   //Read xml file
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(Application.dataPath + "/resources/gameplay.xml");

            LoadTeamsAndPlayers(xmlDoc);
            //ProcessGameplay(xmlDoc);

            // 
            XmlNode rootNode = xmlDoc.DocumentElement;
            //yield return new WaitForSeconds(PlayDelay);
            // yield return new WaitForSeconds(5);
            // StartCoroutine(Delay(rootNode));
             ProcessXML(rootNode);
        }
        else
        {
            Debug.Log("Program exit with no data");
        }  
    }
   

    private void ProcessXML(XmlNode someNode)
    {
        //StartCoroutine(Delay());
        //StartCoroutine(PauseForKeypress()); 


        if (someNode.NodeType == XmlNodeType.Text)
        {
            Debug.Log(someNode.NodeType + " is " + someNode.Value);
        }
        else
        {
            Debug.Log(someNode.NodeType + " is " + someNode.Name);
            NodeProcessor(someNode);
        }

        // Get the attributes of a node
        if (someNode.Attributes != null)
        {
            XmlAttributeCollection someNodeAttributes = someNode.Attributes;
            foreach (XmlAttribute anAttribute in someNodeAttributes)
            {
                Debug.Log("     Attribute:" + anAttribute.Name + " with a value of: " + anAttribute.Value);
                switch (anAttribute.Name.ToLower())
                {
                    case ("player"):
                        {
                            batter = Int32.Parse(anAttribute.Value);
                            txtBatter.text = players[batter].playerName; 
                            break;
                        }
                    case ("type"):
                        {
                            if (anAttribute.Value=="walk")
                            {
                                AdvancePlayer(batter, 1, true);
                            }
                            else if(anAttribute.Value == "bases"){
                                AdvancePlayer(batter, 1, true);
                            }
                            else if (anAttribute.Value == "hit" || anAttribute.Value == "advance")
                            {
                                int bases = Int32.Parse(someNode.ChildNodes[0].InnerText);
                                bool hit = anAttribute.Value == "hit";
                                AdvancePlayer(batter, bases, hit);
                            }
                            else if (anAttribute.Value=="k")
                            {
                                outs++;
                                Outs.text = outs.ToString();
                            }
                            else if (anAttribute.Value=="out")
                            {
                               AdvancePlayer(batter, 0, false);
                            }


                            attributeType = anAttribute.Name;
                            break;
                        }
                    case ("bases"):
                        {
                            AdvancePlayer(batter, Int32.Parse(anAttribute.Value), (attributeType == "hit"));
                            batter = 0;attributeType = "";
                            break;
                        }
                }
            }
        }
        // Get the child nodes / next node
        XmlNodeList nextNodes = someNode.ChildNodes;
        foreach (XmlNode aChildNode in nextNodes)
        {
            Debug.Log("   Getting next node");
            // StartCoroutine(Delay(aChildNode)); 
            ProcessXML(aChildNode);
        }
    }

    private void NodeProcessor(XmlNode nodeName)
    {
        switch (nodeName.Name.ToLower())
        {
            case ("inning"):
                if(nodeName.ChildNodes != null){
                     // same as nodeName.HasChildNodes??
                    if (nodeName.OuterXml == "visitor")
                    {
                        inning++;
                        atBat = 1;
                        outs = 0;
                    } else if (nodeName.OuterXml == "home")
                    {
                        atBat = 2;
                        outs = 0;
                    }
                }
                Debug.Log("inning");
                break;
            case ("play"):
                Debug.Log("play");
                if (nodeName.HasChildNodes)
                {
                    XmlNodeList nextPlayNodes = nodeName.ChildNodes;
                    foreach (XmlNode playChildNode in nextPlayNodes)
                    {
                        switch (playChildNode.Name)
                        {
                            case ("hit"):
                                int bases = 0;
                                if (playChildNode.HasChildNodes)
                                {
                                    bases = Int32.Parse(playChildNode.ChildNodes[0].Value);
                                }
                                
                                int playerNumber = Int32.Parse(playChildNode.InnerXml);
                                AdvancePlayer(playerNumber,bases,true);
                                break;
                            case ("advance"):
                                break;
                           // case ("bases"):
                            //    break;
                        }
                    }
                }
                break;

            //default:
            //    Debug.Log("default");
            //    break;
        }
    }
    // Move a player around the bases
  private void UpdateScore()
    {
        if (atBat == 1)
        {
            vScore++;
            VisitorScore.text = vScore.ToString();
        }
        else
        { 
            hScore++;
            HomeScore.text = hScore.ToString();
        }
    } 
    
    private void AdvancePlayer(int playerNum, int bases, bool hit)
    {
        // This is the batter running to base, not just a player advancing
        if (hit)
        {
            // Check for a home run, clear the bases, score the runners
            if (bases > 3)
            {
                UpdateScore();
                // go through the bases and advance the player(s)
                for (int x = 1; x < 4; ++x)
                {
                    if (structBase[bases].onIndex > 0)
                    {
                        structBase[bases].onIndex = 0;
                        UpdateScore();
                    }
                }
            }
                if (structBase[bases].onIndex > 0)
            {  // There is already someone on the base
                //structBase[bases].playerWasOn = structBase[bases].playerOn;
                structBase[bases].wasOnIndex = structBase[bases].onIndex;
                structBase[bases].onIndex = playerNum;
            }
            else
            {
                structBase[bases].onIndex = playerNum;
            }
        }
        else
        { // Advance a player, or they are out
            bool foundPlayer = false;
            // go through the bases and find a player, then advance or out
            for (int x = 1; x < 4; ++x)
            {   // this was not a hit, just a player advancing, 
                // so remove the player from their current base and move them to a new base
                if (foundPlayer == false)
                {
                    if (structBase[x].onIndex == playerNum)
                    {
                        foundPlayer = true;

                        // Check to see if the player should score
                        if (x + bases > 3)
                        {
                            structBase[x].onIndex = 0;
                            UpdateScore();
                            // keep tract of the stats if the player scores
                        }
                        else if (bases == 0)
                        { // the player is out
                            structBase[x].onIndex = 0;
                        }
                        else
                        { // The player won't score, so advance to the correct base
                            if (structBase[x + bases].onIndex > 0)
                            { // There is already someone on the base
                                if (structBase[x + bases].wasOnIndex > 0)
                                {
                                    Debug.Log("Error - Should not make it here.  There is already someone who was on base.");
                                }
                                else // move the player already on the base to the side, 
                                {   //put the player on the new base, clear the original base
                                    structBase[x + bases].wasOnIndex = structBase[x + bases].onIndex;
                                    structBase[x + bases].onIndex = playerNum;
                                    structBase[x].onIndex = 0;
                                }
                            }
                            else
                            { // Nobody on this base, move the player and clear the originating base
                                structBase[x + bases].onIndex = playerNum;
                                structBase[x].onIndex = 0;
                            }
                        }
                    }
                    else if (structBase[x].wasOnIndex == playerNum)
                    {
                        foundPlayer = true;
                        // Check to see if the player should score
                        if (x + bases > 3)
                        {
                            structBase[x].wasOnIndex = 0;
                            UpdateScore();
                            // keep tract of the stats if the player scores
                        }
                        // not scoring, so advance the player
                        else
                        {
                            // check the base they are going to
                            if (structBase[x + bases].wasOnIndex > 0)
                            { // There is already someone on the base
                                if (structBase[x + bases].wasOnIndex > 0)
                                {
                                    Debug.Log("Error - Should not make it here.  There is already someone who was on base.");
                                }
                                else
                                {
                                    structBase[x + bases].wasOnIndex = playerNum;
                                    structBase[x].wasOnIndex = 0;
                                }
                            }
                            else
                            { // Nobody on this base, move the player and clear the original base
                                structBase[x + bases].onIndex = playerNum;
                                structBase[x].wasOnIndex = 0;
                            }
                        }
                    }
                }
            }
            if (foundPlayer)
            {

            }
            else
            {

            }
        }
    }

    // Get both teams, and their players, 
    // display the team names on the scoreboard
    private void LoadTeamsAndPlayers(XmlDocument xmlGameData)
    {
        //XmlNodeList gameDetailNodes = xmlGameData.SelectNodes("//game/details");
        // Gets both teams
       
        XmlNode teamVisitor = xmlGameData.SelectSingleNode("//game/details/visitor/teamName");
        txtVisitingTeam.text = teamVisitor.InnerText;
        if (teamVisitor.InnerText.Length > 15) txtVisitingTeam.fontSize = 10;
        
        XmlNodeList teamPlayerNodeList = xmlGameData.SelectNodes("//game/details/visitor/players/player");

        //TeamVisitors[] saVisitors = new TeamVisitors[teamPlayerNodeList.Count];
        //TeamVisitors[] saVisitors = new TeamVisitors[2, teamPlayerNodeList.Count];
        //Players[] structPlayers = new Players[2, teamPlayerNodeList.Count];

        int x = 0;
        foreach (XmlNode player in teamPlayerNodeList)
        {
            Debug.Log(player.Name);
            if (player.Name.Equals("teamname"))
            {
                //save team name
            }
            else
            {
               // saVisitors[x++].playerName = player.InnerText;
                players[++x].playerName = player.InnerText;
            } 
        }

        XmlNode teamHome = xmlGameData.SelectSingleNode("//game/details/home/teamName");
        txtHomeTeam.text = teamHome.InnerText;
        if (teamHome.InnerText.Length > 15) txtHomeTeam.fontSize = 10; 

        teamPlayerNodeList = xmlGameData.SelectNodes("//game/details/home/players/player");

        //TeamHome[] saHome = new TeamHome[teamPlayerNodeList.Count];
        x = 0;
        foreach (XmlNode player in teamPlayerNodeList)
        {
            Debug.Log(player.Name);
            if (player.Name.Equals("teamname"))
            {
                //save team name
            }
            else
            {
                // saHome[x++].playerName = player.InnerText;
                players[++x].playerName = player.InnerText;
            }
        }
        //XmlNode teamVisitor = teamVisitorNodeList;

        //XmlNode teamVisitor = gameDetailNodes.
        //XmlNode teamHome = teamVisitor.FirstChild;
        Debug.Log(teamVisitor.Attributes);
        Debug.Log(teamVisitor.InnerText);
       
    }

    private void ProcessGameplay(XmlDocument xmlData)
    {
        int gameInning = 0;
        int teamAtBat = 0;
        TextAsset xmlAssetFile = Resources.Load<TextAsset>("gameplay");
        XDocument allXmlParams = XDocument.Parse(xmlAssetFile.text);
        //var xmlPlayActions = allXmlParams.Element("game").Element("actions");

       // XmlNodeList gamePlayDetailNodes = xmlData.SelectNodes("//game/actions");
        XmlNodeList gamePlayDetailNodes = xmlData.GetElementsByTagName("actions");
        XmlNode nextNode;
        XmlNode testNode = xmlData.DocumentElement;

        foreach (XmlNode gamePlay in gamePlayDetailNodes)
        { 
            switch (gamePlay.Name)
            {
                case ("play"):
                    Debug.Log("play");
                    break;

                case ("inning"):
                    gameInning = Int32.Parse(gamePlay.Value);
                    Debug.Log("inning");
                    break;

                case ("actions"):
                    Debug.Log("Actions");
                    break;

                default:
                    Debug.Log("default");
                    break;
            }
           //string xmlLable = gamePlay.Value;//  play.Attributes[0].InnerText;
           // Debug.Log(xmlLable);
        }
    }

    private bool CheckIfDataExists()
    {
        if (File.Exists(Application.dataPath + "/resources/gameplay.xml"))
        {
            return true;
        }
        else
        {
            Debug.Log("XML file not found");
            return false;
        }
    }
    
    private void LoadGamePlayData()
    {
    //    TextAsset textAsset = TextAsset.Resources.Load("gameplay");
    //    XmlDocument xmlDocument = new XmlDocument();
    //    foreach (XmlNode play in xmlDocument[""])
    }
    IEnumerator Delay()
    {
        //print(Time.time);
       // yield return new WaitForSeconds(PlayDelay);
        yield return new WaitForSeconds(3);
        //print(Time.time);
        
    }
    IEnumerator PauseForKeypress()
    {
        //wait for space to be pressed
        yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.Space));
    }

}