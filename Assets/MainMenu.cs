﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    // Start is called before the first frame update
  public void Scorecard()
    {
        SceneManager.LoadScene(3);
    }
    public void Replay()
    {
        SceneManager.LoadScene(2);
    }
    public void QuitApp()
    {
        Debug.Log("Quit Button Pressed!");
        Application.Quit();
    }
}
